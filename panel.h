#ifndef STEGAPANEL_H
#define STEGAPANEL_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QSlider>
#include <QShortcut>
#include <QMessageBox>
#include <string>

using namespace std ;

namespace Ui {
class StegaPanel;
}

class StegaPanel : public QMainWindow
{
    Q_OBJECT

public:
    explicit StegaPanel(QWidget *parent = 0);
    void hideMessage();
    void revealMessage();
    ~StegaPanel();

private slots:
    void on_loadImage_clicked();
    void on_hideMessage_clicked();
    void on_getMessage_clicked();

    void on_storeImage_clicked();

private:
    Ui::StegaPanel *ui;
    QImage old_image;
    QImage new_image;
    int leastBits; //number of bits used in pixel for message
    QString orig_msg;

};

#endif // MAINWINDOW_H
